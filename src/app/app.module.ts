import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { FooterComponent } from "./footer/footer.component";
import { HeaderComponent } from "./header/header.component";
import { ClienteComponent } from "./cliente/cliente.component";
import { ClienteService } from "./cliente/cliente.service";
import { JuegoComponent } from "./juego/juego.component";
import { RouterModule, Routes } from "@angular/router";
import { JuegoService } from "./juego/juego.service";
import { FormClienteComponent } from "./cliente/formCliente.component";
import { FormTiendaComponent } from "./tienda/formTienda.component";
import { FormsModule } from "@angular/forms";
import { TiendaComponent } from "./tienda/tienda.component";
import { TiendaService } from "./tienda/tienda.service";
import { CompanyComponent } from "./company/company.component";
import { CompanyService } from "./company/company.service";
import { FormCompanyComponent } from "./company/formCompany.component";
import { FormJuegoComponent } from "./juego/formJuego.component";
import { HomeComponent } from "./home/home.component";
import { PedidosComponent } from "./pedidos/pedidos.component";
import { PedidoService } from "./pedidos/pedido.service";
import { LoginComponent } from "./login/login.component";
import { LoginService } from "./login/login.service";
import { FormPedidoComponent } from "./pedidos/formPedido.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { PaginatePipe } from "./pipes/paginator.pipe";
import { MatPaginatorModule } from "@angular/material/paginator";

const ROUTES: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: "login", component: LoginComponent },
  { path: "home", component: HomeComponent },
  { path: "juego", component: JuegoComponent },
  { path: "juego/form", component: FormJuegoComponent },
  { path: "juego/form/:juegoRef", component: FormJuegoComponent },
  { path: "cliente", component: ClienteComponent },
  { path: "tienda", component: TiendaComponent },
  { path: "cliente/form", component: FormClienteComponent },
  { path: "cliente/form/:documentoID", component: FormClienteComponent },
  { path: "tienda/form", component: FormTiendaComponent },
  { path: "tienda/form/:name", component: FormTiendaComponent },
  { path: "company", component: CompanyComponent },
  { path: "company/form", component: FormCompanyComponent },
  { path: "company/form/:cif", component: FormCompanyComponent },
  { path: "pedido", component: PedidosComponent },
  { path: "pedido/form", component: FormPedidoComponent },
  { path: "pedido/form/:pedidoID", component: FormPedidoComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    ClienteComponent,
    JuegoComponent,
    FormClienteComponent,
    FormTiendaComponent,
    TiendaComponent,
    CompanyComponent,
    FormCompanyComponent,
    FormJuegoComponent,
    HomeComponent,
    PedidosComponent,
    FormPedidoComponent,
    LoginComponent,
    PaginatePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(ROUTES),
    MatPaginatorModule,
    BrowserAnimationsModule
  ],
  providers: [
    ClienteService,
    JuegoService,
    TiendaService,
    CompanyService,
    PedidoService,
    LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
