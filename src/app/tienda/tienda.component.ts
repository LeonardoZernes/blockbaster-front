import { Component, OnInit } from "@angular/core";
import { Tienda } from "./tienda";
import { TiendaService } from "./tienda.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-tienda",
  templateUrl: "./tienda.component.html",
  styleUrls: ["./tienda.component.css"]
})
export class TiendaComponent implements OnInit {
  public habilitar: boolean = true;

  setHabilitar(): void {
    this.habilitar = !this.habilitar;
  }

  public tiendas: Tienda[];
  constructor(private tiendaService: TiendaService) {}

  ngOnInit(): void {
    this.tiendaService
      .getTiendas()
      .subscribe(tiendas => (this.tiendas = tiendas));
  }

  delete(tienda: Tienda): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success m-2",
        cancelButton: "btn btn-danger m-2"
      },
      buttonsStyling: false
    });

    swalWithBootstrapButtons
      .fire({
        title: "¿Está seguro?",
        text:
          "Esta operación no se puede deshacer, ¿quiere eliminar la tienda?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Confirmar",
        cancelButtonText: "Cancelar",
        reverseButtons: true
      })
      .then(result => {
        if (result.value) {
          this.tiendaService.delete(tienda.name).subscribe(response => {
            this.tiendas = this.tiendas.filter(tiend => tiend !== tienda);
            swalWithBootstrapButtons.fire(
              "Tienda eliminada",
              "Tienda se ha eliminado con éxito",
              "success"
            );
          });
        }
      });
  }
}
