import { Component, OnInit } from "@angular/core";
import { Tienda } from "./tienda";
import { TiendaService } from "./tienda.service";
import { Router, ActivatedRoute } from "@angular/router";
import { switchAll } from "rxjs/operators";
import swal from "sweetalert2";

@Component({
  selector: "app-form",
  templateUrl: "./formTienda.component.html"
})
export class FormTiendaComponent implements OnInit {
  public nuevaTienda: boolean = true;

  changeNuevaTienda(): void {
    this.nuevaTienda = !this.nuevaTienda;
  }

  public creaTienda: string = "Añadir Tienda";
  public editaTienda: string = "Modificar datos de la tienda";
  public tienda: Tienda = new Tienda();

  constructor(
    private tiendaService: TiendaService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.cargarTienda();
  }

  public cargarTienda(): void {
    this.activatedRoute.params.subscribe(params => {
      let name = params["name"];
      if (name) {
        this.changeNuevaTienda();
        this.tiendaService
          .getTienda(name)
          .subscribe(tienda => (this.tienda = tienda));
      }
    });
  }

  public create(): void {
    this.tiendaService.create(this.tienda).subscribe(reponse => {
      this.router.navigate(["/tienda"]);
      swal.fire("Nueva Tienda", "Tienda añadida correctamente", "success");
    });
  }

  public update(): void {
    this.tiendaService.update(this.tienda).subscribe(response => {
      this.router.navigate(["/tienda"]);
      swal.fire(
        "Tienda editada",
        "La tienda ha sido modificada con éxtio",
        "success"
      );
    });
  }
}
