import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { Tienda } from "./tienda";
import { Router } from "@angular/router";
import Swal from "sweetalert2";

@Injectable({
  providedIn: "root"
})
export class TiendaService {
  private urlEndPoint: string = "http://localhost:8090/tienda";

  private httpHeaders = new HttpHeaders({ "Content-Type": "application/json" });

  constructor(private http: HttpClient, private router: Router) {}

  getTiendas(): Observable<Tienda[]> {
    return this.http.get(this.urlEndPoint + "/list").pipe(
      map(response => response as Tienda[]),
      catchError(e => {
        console.log(e);
        Swal.fire("Error al conectar con el servidor", e.message, "error");
        return throwError(e);
      })
    );
  }

  getTienda(name: string): Observable<Tienda> {
    return this.http.get<Tienda>(this.urlEndPoint + `?name=${name}`).pipe(
      catchError(e => {
        this.router.navigate(["/tienda"]);
        console.log(e.error.message);
        Swal.fire("Error al editar", e.error.message, "error");
        return throwError(e);
      })
    );
  }

  create(tienda: Tienda): Observable<Tienda> {
    return this.http
      .post<Tienda>(this.urlEndPoint, tienda, {
        headers: this.httpHeaders
      })
      .pipe(
        catchError(e => {
          console.log(e.error.message);
          Swal.fire("Error al crear la tienda", e.error.message, "error");
          return throwError(e);
        })
      );
  }

  update(tienda: Tienda): Observable<Tienda> {
    return this.http.put<Tienda>(this.urlEndPoint, tienda, {
      headers: this.httpHeaders
    });
  }

  delete(name: string): Observable<Tienda> {
    return this.http.delete<Tienda>(this.urlEndPoint + `?name=${name}`, {
      headers: this.httpHeaders
    });
  }
}
