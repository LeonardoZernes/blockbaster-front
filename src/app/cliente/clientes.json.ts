import { Cliente } from "./cliente";

export const CLIENTES: Cliente[] = [
  {
    nombre: "Adrian",
    correo: "adrianfernndez@gmail.com",
    documentoID: "79085612C",
    fechaNac: "26/04/1997",
    password: "12345678",
    roles: ["USER", "ADMIN"]
  },
  {
    nombre: "Luisa",
    correo: "luisiGuapi@gmail.com",
    documentoID: "28043416Q",
    fechaNac: "11/05/1983",
    password: "12345678",
    roles: ["USER"]
  },
  {
    nombre: "Marta",
    correo: "marta_TKM@gmail.com",
    documentoID: "46780321K",
    fechaNac: "09/02/1993",
    password: "12345678",
    roles: ["USER"]
  },
  {
    nombre: "Gabriela",
    correo: "gabilolazo19@hotmail.com",
    documentoID: "79023167H",
    fechaNac: "02/07/1991",
    password: "12345678",
    roles: ["USER"]
  }
];
