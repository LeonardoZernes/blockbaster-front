import { Component, OnInit } from "@angular/core";
import { Cliente } from "./cliente";
import { ClienteService } from "./cliente.service";
import { PageEvent } from "@angular/material/paginator";
import Swal from "sweetalert2";

@Component({
  selector: "app-cliente",
  templateUrl: "./cliente.component.html",
  styleUrls: ["./cliente.component.css"]
})
export class ClienteComponent implements OnInit {
  public habilitar: boolean = true;

  setHabilitar() {
    this.habilitar = !this.habilitar;
  }

  page_size: number = 5;
  page_number: number = 1;
  pageSizeOpt = [5, 10, 50, 100];

  handlePage(e: PageEvent) {
    this.page_size = e.pageSize;
    this.page_number = e.pageIndex + 1;
  }

  public clientes: Cliente[] = [];

  constructor(private clienteService: ClienteService) {}

  ngOnInit(): void {
    this.clienteService
      .getClientes()
      .subscribe(clientes => (this.clientes = clientes));
  }

  delete(cliente: Cliente): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success m-2",
        cancelButton: "btn btn-danger m-2"
      },
      buttonsStyling: false
    });

    swalWithBootstrapButtons
      .fire({
        title: "¿Está seguro?",
        text:
          "Esta operación no se puede deshacer, ¿quiere eliminar al cliente?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Confirmar",
        cancelButtonText: "Cancelar",
        reverseButtons: true
      })
      .then(result => {
        if (result.value) {
          this.clienteService
            .delete(cliente.documentoID)
            .subscribe(response => {
              this.clientes = this.clientes.filter(cli => cli !== cliente);
              swalWithBootstrapButtons.fire(
                "Cliente eliminado",
                "Cliente se ha eliminado con éxito",
                "success"
              );
            });
        } /* Este bloque si queremos mostrar mensaje al cancelar
          else if (
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            "Cancelado",
            "No se han realizado acciones.",
            "error"
          );
        } */
      });
  }
}
