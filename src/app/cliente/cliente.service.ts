import { Injectable } from "@angular/core";
import { CLIENTES } from "./clientes.json";
import { Cliente } from "./cliente";
import { of, Observable, throwError } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";
import { Router } from "@angular/router";
import Swal from "sweetalert2";
import { formatDate } from "@angular/common";

@Injectable()
export class ClienteService {
  private urlEndPoint: string = "http://localhost:8090/cliente/";

  private httpHeaders = new HttpHeaders({ "Content-Type": "application/json" });

  constructor(private http: HttpClient, private router: Router) {}

  getClientes(): Observable<Cliente[]> {
    return (
      this.http
        .get<Cliente[]>(this.urlEndPoint + "list")
        /* .pipe(
        map(response => {
          let clientes = response as Cliente[];
          return clientes.map(cliente => {
            cliente.fechaNac = formatDate(
              cliente.fechaNac,
              "dd/MM/yyyy",
              "en-US"
            );
            return cliente;
          });
        })
      ) */
        .pipe(
          catchError(e => {
            console.log(e);
            Swal.fire("Error al conectar con el servidor", e.message, "error");
            return throwError(e);
          })
        )
    );
    //return this.http.get<Cliente[]>(this.urlEndPoint);
  }

  getCliente(documentoID: string): Observable<Cliente> {
    return this.http
      .get<Cliente>(this.urlEndPoint + `?documentoID=${documentoID}`)
      .pipe(
        catchError(e => {
          this.router.navigate(["/cliente"]);
          console.log(e.error.message);
          Swal.fire("Error al editar", e.error.message, "error");
          return throwError(e);
        })
      );
  }

  create(cliente: Cliente): Observable<Cliente> {
    return this.http
      .post<Cliente>(this.urlEndPoint, cliente, {
        headers: this.httpHeaders
      })
      .pipe(
        catchError(e => {
          console.log(e.error.message);
          Swal.fire("Error al crear al cliente", e.error.message, "error");
          return throwError(e);
        })
      );
  }

  update(cliente: Cliente): Observable<Cliente> {
    return this.http.put<Cliente>(
      this.urlEndPoint + `?documentoID=${cliente.documentoID}`,
      cliente,
      { headers: this.httpHeaders }
    );
  }

  delete(documentoID: string): Observable<Cliente> {
    return this.http.delete<Cliente>(
      this.urlEndPoint + `?documentoID=${documentoID}`,
      { headers: this.httpHeaders }
    );
  }
}
