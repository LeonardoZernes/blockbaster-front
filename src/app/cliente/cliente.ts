export class Cliente {
  documentoID: string;
  nombre: string;
  correo: string;
  fechaNac: string;
  password: string;
  roles: string[];
}
