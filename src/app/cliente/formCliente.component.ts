import { Component, OnInit } from "@angular/core";
import { Cliente } from "./cliente";
import { ClienteService } from "./cliente.service";
import { Router, ActivatedRoute } from "@angular/router";
import swal from "sweetalert2";

@Component({
  selector: "app-form",
  templateUrl: "./formCliente.component.html"
})
export class FormClienteComponent implements OnInit {
  // Variable para evaluar si se crea o edita cliente
  public nuevoCliente: boolean = true;

  changeNuevoCliente(): void {
    this.nuevoCliente = !this.nuevoCliente;
  }

  public showPass: string = "password";
  public showButton: boolean = true;

  changeShowPass(): void {
    if (this.showPass == "password") {
      this.showPass = "text";
      this.showButton = false;
    } else if (this.showPass == "text") {
      this.showPass = "password";
      this.showButton = true;
    }
  }

  public creaCliente: string = "Crear Cliente";
  public editaCliente: string = "Modificar datos de cliente";
  public cliente: Cliente = new Cliente();
  public today: Date = new Date();

  constructor(
    private clienteService: ClienteService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.cargarCliente();
  }

  public cargarCliente(): void {
    this.activatedRoute.params.subscribe(params => {
      let documentoID = params["documentoID"];
      if (documentoID) {
        // Si editamos un cliente, activamos la variable para mostrar editar
        this.changeNuevoCliente();
        this.clienteService
          .getCliente(documentoID)
          .subscribe(cliente => (this.cliente = cliente));
      }
    });
  }

  public create(): void {
    this.clienteService.create(this.cliente).subscribe(response => {
      this.router.navigate(["/cliente"]);
      swal.fire("Nuevo cliente", "Cliente creado con éxito", "success");
    });
  }

  public update(): void {
    this.clienteService.update(this.cliente).subscribe(response => {
      this.router.navigate(["/cliente"]);
      swal.fire("Cliente editado", "Cliente modificado con éxito", "success");
    });
  }
}
