import { Component, OnInit } from "@angular/core";
import { Company } from "./company";
import { CompanyService } from "./company.service";
import { Router, ActivatedRoute } from "@angular/router";
import Swal from "sweetalert2";

@Component({
  selector: "app-form-company",
  templateUrl: "./formCompany.component.html"
})
export class FormCompanyComponent implements OnInit {
  public nuevaCompany: boolean = true;

  changeNuevaCompany(): void {
    this.nuevaCompany = !this.nuevaCompany;
  }

  public creaCompany: string = "Crear Compañía";
  public editaCompany: string = "Modificar datos de la compañía";
  public company: Company = new Company();

  constructor(
    private companyService: CompanyService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.cargarCompany();
  }

  public cargarCompany(): void {
    this.activatedRoute.params.subscribe(params => {
      let cif = params["cif"];
      if (cif) {
        this.changeNuevaCompany();
        this.companyService
          .getCompany(cif)
          .subscribe(company => (this.company = company));
      }
    });
  }

  public create(): void {
    this.companyService.create(this.company).subscribe(response => {
      this.router.navigate(["/company"]);
      Swal.fire("Nueva compañía", "Compañía añadida con éxito", "success");
    });
  }

  public update(): void {
    this.companyService.update(this.company).subscribe(response => {
      this.router.navigate(["/company"]);
      Swal.fire("Compañía editada", "Compañía modificada con éxito", "success");
    });
  }
}
