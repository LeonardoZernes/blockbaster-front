import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Observable, throwError } from "rxjs";
import { Company } from "./company";
import Swal from "sweetalert2";
import { catchError } from "rxjs/operators";

@Injectable()
export class CompanyService {
  private urlEndPoint: string = "http://localhost:8090/company";

  private httpHeaders = new HttpHeaders({ "Content-Type": "application/json" });

  constructor(private http: HttpClient, private router: Router) {}

  getCompanies(): Observable<Company[]> {
    return this.http.get<Company[]>(this.urlEndPoint + "/list").pipe(
      catchError(e => {
        console.log(e);
        Swal.fire("Error al conectar con el servidor", e.message, "error");
        return throwError(e);
      })
    );
  }

  getCompany(cif: String): Observable<Company> {
    return this.http.get<Company>(this.urlEndPoint + `?cif=${cif}`).pipe(
      catchError(e => {
        this.router.navigate(["/company"]);
        console.log(e.error.message);
        Swal.fire("Error al editar", e.error.message, "error");
        return throwError(e);
      })
    );
  }

  create(company: Company): Observable<Company> {
    return this.http
      .post<Company>(this.urlEndPoint, company, { headers: this.httpHeaders })
      .pipe(
        catchError(e => {
          console.log(e.error.message);
          Swal.fire("Error al crear la compañía", e.error.message, "error");
          return throwError(e);
        })
      );
  }

  update(company: Company): Observable<Company> {
    return this.http.put<Company>(this.urlEndPoint, company, {
      headers: this.httpHeaders
    });
  }

  delete(cif: String): Observable<Company> {
    return this.http.delete<Company>(this.urlEndPoint + `?cif=${cif}`, {
      headers: this.httpHeaders
    });
  }
}
