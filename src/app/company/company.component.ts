import { Component, OnInit } from "@angular/core";
import { Company } from "./company";
import { CompanyService } from "./company.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-company",
  templateUrl: "./company.component.html",
  styleUrls: ["./company.component.css"]
})
export class CompanyComponent implements OnInit {
  public habilitar: boolean = true;

  setHabilitar() {
    this.habilitar = !this.habilitar;
  }

  public companies: Company[];

  constructor(private companyService: CompanyService) {}

  ngOnInit(): void {
    this.companyService
      .getCompanies()
      .subscribe(companies => (this.companies = companies));
  }

  delete(company: Company): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success m-2",
        cancelButton: "btn btn-danger m-2"
      },
      buttonsStyling: false
    });

    swalWithBootstrapButtons
      .fire({
        title: "¿Está seguro?",
        text:
          "Esta operación no se puede deshacer, ¿quiere eliminar la compañía?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Confirmar",
        cancelButtonText: "Cancelar",
        reverseButtons: true
      })
      .then(result => {
        if (result.value) {
          this.companyService.delete(company.cif).subscribe(response => {
            this.companies = this.companies.filter(comp => comp !== company);
            swalWithBootstrapButtons.fire(
              "Compañia eliminada",
              "Compañía se ha eliminado con éxito",
              "success"
            );
          });
        }
      });
  }
}
