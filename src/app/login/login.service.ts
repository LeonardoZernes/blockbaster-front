import { Injectable } from "@angular/core";
import { HttpHeaders } from "@angular/common/http";

@Injectable()
export class LoginService {
  public credentials: any;

  constructor() {}

  save(credentials: any): void {
    //this.credentials = credentials;
    localStorage.setItem(
      "authToken",
      btoa(`${this.credentials.username}:${this.credentials.password}`)
    );
  }

  getAuthHeaders(): HttpHeaders {
    let token = localStorage.getItem("authToken");
    if (!token) {
      return null;
    }
    return new HttpHeaders({
      Authorization: "Basic " + token
    });
    /* if (!this.credentials){
      return null;
    }
    return new HttpHeaders({
      'Authorization':
        "Basic " +
        btoa(`${this.credentials.username}:${this.credentials.password}`)
    });
  }*/
  }
}
