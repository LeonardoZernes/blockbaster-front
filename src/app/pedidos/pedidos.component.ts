import { Component, OnInit } from "@angular/core";
import { Pedido } from "./pedido";
import { Tienda } from "../tienda/tienda";
import { Juego } from "../juego/juego";
import { Cliente } from "../cliente/cliente";
import { PedidoService } from "./pedido.service";
import { JuegoService } from "../juego/juego.service";
import { TiendaService } from "../tienda/tienda.service";
import { ClienteService } from "../cliente/cliente.service";
import { Observable } from "rxjs";
import Swal from "sweetalert2";

@Component({
  selector: "app-pedidos",
  templateUrl: "./pedidos.component.html",
  styleUrls: ["./pedidos.component.css"]
})
export class PedidosComponent implements OnInit {
  public habilitar: boolean = true;
  public validado: boolean = false;

  setHabilitar(): void {
    this.habilitar = !this.habilitar;
  }

  empty(): boolean {
    return !Array.isArray(this.pedidos);
  }

  public pedidos: Pedido[] = [];
  public busqueda: Cliente = new Cliente();
  public clientes: Cliente[];
  public juegos: Juego[];
  public tiendas: Tienda[];

  constructor(
    private pedidoService: PedidoService,
    private juegoService: JuegoService,
    private tiendaService: TiendaService,
    private clienteService: ClienteService
  ) {}

  ngOnInit(): void {
    this.juegoService.getJuegos().subscribe(juegos => (this.juegos = juegos));
    this.tiendaService
      .getTiendas()
      .subscribe(tiendas => (this.tiendas = tiendas));
    this.clienteService
      .getClientes()
      .subscribe(clientes => (this.clientes = clientes));
  }

  getPedidos(documentoID: string): void {
    this.pedidoService
      .getPedidos(documentoID)
      .subscribe(pedidos => (this.pedidos = pedidos));
    this.validado = true;
  }

  delete(pedido: Pedido): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success m-2",
        cancelButton: "btn btn-danger m-2"
      },
      buttonsStyling: false
    });

    swalWithBootstrapButtons
      .fire({
        title: "¿Está seguro?",
        text:
          "Esta operación no se puede deshacer, ¿quiere eliminar el registro del pedido?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Confirmar",
        cancelButtonText: "Cancelar",
        reverseButtons: true
      })
      .then(result => {
        if (result.value) {
          this.pedidoService.delete(pedido.pedidoID).subscribe(response => {
            this.pedidos = this.pedidos.filter(ped => ped !== pedido);
            swalWithBootstrapButtons.fire(
              "Pedido eliminado",
              "Pedido se ha eliminado con éxito",
              "success"
            );
          });
        }
      });
  }
}
