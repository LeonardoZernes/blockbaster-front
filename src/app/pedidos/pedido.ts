import { Cliente } from "../cliente/cliente";
import { Tienda } from "../tienda/tienda";
import { Juego } from "../juego/juego";

export class Pedido {
  pedidoID: number;
  fechCreate: Date;
  state: string;
  cliente: Cliente;
  tienda: Tienda;
  gameRef: Juego;
}
