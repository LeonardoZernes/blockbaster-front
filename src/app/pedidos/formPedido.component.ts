import { Component, OnInit } from "@angular/core";
import { Pedido } from "./pedido";
import { Cliente } from "../cliente/cliente";
import { Tienda } from "../tienda/tienda";
import { Juego } from "../juego/juego";
import { JuegoService } from "../juego/juego.service";
import { Router, ActivatedRoute } from "@angular/router";
import { ClienteService } from "../cliente/cliente.service";
import { TiendaService } from "../tienda/tienda.service";
import { PedidoService } from "./pedido.service";
import Swal from "sweetalert2";
import { formatDate } from "@angular/common";
import { map } from "rxjs/operators";

@Component({
  selector: "app-form-juego",
  templateUrl: "./formPedido.component.html"
})
export class FormPedidoComponent implements OnInit {
  public nuevoPedido: boolean = true;

  changeNuevoPedido(): void {
    this.nuevoPedido = !this.nuevoPedido;
  }

  public creaPedido: string = "Nuevo Pedido";
  public editaPedido: string = "Modificar datos del pedido";
  public pedido: Pedido = new Pedido();
  public juegos: Juego[];
  public tiendas: Tienda[];
  public clientes: Cliente[] = [];
  public estados: string[] = ["VENDIDO", "ALQUILADO"];

  constructor(
    private clienteService: ClienteService,
    private tiendaService: TiendaService,
    private juegoService: JuegoService,
    private pedidoService: PedidoService,
    private router: Router,
    private activedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.clienteService
      .getClientes()
      .subscribe(clientes => (this.clientes = clientes));
    this.tiendaService
      .getTiendas()
      .subscribe(tiendas => (this.tiendas = tiendas));
    this.juegoService.getJuegos().subscribe(juegos => (this.juegos = juegos));

    this.cargarPedido();
  }

  // Se intentó cambiar la fecha de los clientes para hacer bien los pedidos pero no funcionó
  // Daba error al traer clientes 26/04/1997 cuando esperaba 1997-04-26 para hacer POST
  /* public changeFecha(): void {
    this.clientes.forEach(cliente => {
      cliente.fechaNac = formatDate(cliente.fechaNac, "yyyy-MM-dd", "en-US");
    });
  } */
  public compareCliente(c1: Cliente, c2: Cliente): boolean {
    return c1 == null || c2 == null ? false : c1.nombre == c2.nombre;
  }

  public compareTienda(t1: Tienda, t2: Tienda): boolean {
    return t1 == null || t2 == null ? false : t1.name == t2.name;
  }

  public compareJuego(j1: Juego, j2: Juego): boolean {
    return j1 == null || j2 == null ? false : j1.title == j2.title;
  }

  public cargarPedido(): void {
    this.activedRoute.params.subscribe(params => {
      let pedidoID = params["pedidoID"];
      if (pedidoID) {
        this.changeNuevoPedido();
        this.pedidoService
          .getPedido(pedidoID)
          .subscribe(pedido => (this.pedido = pedido));
      }
    });
  }

  public create(): void {
    this.pedidoService.create(this.pedido).subscribe(response => {
      this.router.navigate(["/pedido"]);
      Swal.fire(
        "Nuevo pedido",
        "Se ha realizado el pedido correctamente",
        "success"
      );
    });
  }

  public update(): void {
    this.pedidoService.update(this.pedido).subscribe(response => {
      this.router.navigate(["/pedido"]);
      Swal.fire("Pedido editado", "Pedido modificado correctamente", "success");
    });
  }
}
