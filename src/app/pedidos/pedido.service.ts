import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";
import Swal from "sweetalert2";
import { Pedido } from "./pedido";

@Injectable()
export class PedidoService {
  private urlEndPoint: string = "http://localhost:8090/pedido";

  private httpHeaders = new HttpHeaders();

  constructor(private http: HttpClient) {}

  getPedidos(documentoID: string): Observable<Pedido[]> {
    return this.http
      .get<Pedido[]>(
        this.urlEndPoint + "/cliente" + `?documentoID=${documentoID}`
      )
      .pipe(
        catchError(e => {
          console.log(e);
          Swal.fire("Error al conectar con el servidor", e.message, "error");
          return throwError(e);
        })
      );
  }

  getPedido(pedidoID: number): Observable<Pedido> {
    return this.http.get<Pedido>(this.urlEndPoint + `?id=${pedidoID}`).pipe(
      catchError(e => {
        console.log(e);
        Swal.fire("Error al conectar con el servidor", e.message, "error");
        return throwError(e);
      })
    );
  }

  create(pedido: Pedido): Observable<Pedido> {
    return this.http
      .post<Pedido>(this.urlEndPoint, pedido, { headers: this.httpHeaders })
      .pipe(
        catchError(e => {
          console.log(e.error.message);
          Swal.fire("Error al crear el juego", e.error.message, "error");
          return throwError(e);
        })
      );
  }

  update(pedido: Pedido): Observable<Pedido> {
    return this.http.put<Pedido>(this.urlEndPoint, pedido, {
      headers: this.httpHeaders
    });
  }

  delete(pedidoID: number): Observable<Pedido> {
    return this.http.delete<Pedido>(this.urlEndPoint + `?id=${pedidoID}`, {
      headers: this.httpHeaders
    });
  }
}
