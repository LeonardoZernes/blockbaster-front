import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";
import { Juego } from "./juego";
import Swal from "sweetalert2";

@Injectable()
export class JuegoService {
  private urlEndPoint: string = "http://localhost:8090/juego";

  private httpHeaders = new HttpHeaders();

  constructor(private http: HttpClient) {}

  getJuegos(): Observable<Juego[]> {
    return this.http
      .get(this.urlEndPoint + "/list")
      .pipe(map(response => response as Juego[]))
      .pipe(
        catchError(e => {
          console.log(e);
          Swal.fire("Error al conectar con el servidor", e.message, "error");
          return throwError(e);
        })
      );
  }

  getJuego(juegoRef: number): Observable<Juego> {
    return this.http.get<Juego>(this.urlEndPoint + `?id=${juegoRef}`).pipe(
      catchError(e => {
        console.log(e);
        Swal.fire("Error al conectar con el servidor", e.message, "error");
        return throwError(e);
      })
    );
  }

  create(juego: Juego): Observable<Juego> {
    return this.http
      .post<Juego>(this.urlEndPoint, juego, {
        headers: this.httpHeaders
      })
      .pipe(
        catchError(e => {
          console.log(e.error.message);
          Swal.fire("Error al crear el juego", e.error.message, "error");
          return throwError(e);
        })
      );
  }

  update(juego: Juego): Observable<Juego> {
    return this.http.put<Juego>(
      this.urlEndPoint + `?id=${juego.juegoRef}`,
      juego,
      { headers: this.httpHeaders }
    );
  }

  delete(juegoRef: number): Observable<Juego> {
    return this.http.delete<Juego>(this.urlEndPoint + `?id=${juegoRef}`, {
      headers: this.httpHeaders
    });
  }
}
