import { Component, OnInit } from "@angular/core";
import { Juego } from "./juego";
import { JuegoService } from "./juego.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-juego",
  templateUrl: "./juego.component.html",
  styleUrls: ["./juego.component.css"]
})
export class JuegoComponent implements OnInit {
  public habilitar: boolean = true;

  setHabilitar(): void {
    this.habilitar = !this.habilitar;
  }

  public juegos: Juego[];

  constructor(private juegoService: JuegoService) {}

  ngOnInit(): void {
    this.juegoService.getJuegos().subscribe(juegos => (this.juegos = juegos));
  }

  delete(juego: Juego): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success m-2",
        cancelButton: "btn btn-danger m-2"
      },
      buttonsStyling: false
    });

    swalWithBootstrapButtons
      .fire({
        title: "¿Está seguro?",
        text:
          "Esta operación no se puede deshacer, ¿quiere eliminar el registro del juego?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Confirmar",
        cancelButtonText: "Cancelar",
        reverseButtons: true
      })
      .then(result => {
        if (result.value) {
          this.juegoService.delete(juego.juegoRef).subscribe(response => {
            this.juegos = this.juegos.filter(jueg => jueg !== juego);
            swalWithBootstrapButtons.fire(
              "Juego eliminado",
              "Juego se ha eliminado con éxito",
              "success"
            );
          });
        }
      });
  }
}
