import { Component, OnInit } from "@angular/core";
import { Juego } from "./juego";
import { JuegoService } from "./juego.service";
import { Router, ActivatedRoute } from "@angular/router";
import Swal from "sweetalert2";
import { Company } from "../company/company";
import { CompanyService } from "../company/company.service";

@Component({
  selector: "app-form-juego",
  templateUrl: "./formJuego.component.html"
})
export class FormJuegoComponent implements OnInit {
  public nuevoJuego: boolean = true;

  changeNuevoJuego(): void {
    this.nuevoJuego = !this.nuevoJuego;
  }

  public creaJuego: string = "Insertar Juego";
  public editaJuego: string = "Modificar datos del juego";
  public juego: Juego = new Juego();
  public companies: Company[];
  public categories: string[] = [
    "SHOOTER",
    "MMO",
    "MOOBA",
    "PLATAFORMAS",
    "SIMULADOR",
    "RPG",
    "ARPG"
  ];

  constructor(
    private companyService: CompanyService,
    private juegoService: JuegoService,
    private router: Router,
    private activedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.companyService
      .getCompanies()
      .subscribe(response => (this.companies = response));
    this.cargarJuego();
  }

  public compareCompany(c1: Company, c2: Company): boolean {
    // Función para comparar las companies del juego con la lista y marcar
    // aquellas que sean del juego.
    return c1 == null || c2 == null ? false : c1.name == c2.name;
  }

  // Esta de pegi no hace falta porque se compara el mismo valor del campo con el value
  public comparePegi(p1: number, p2: number): boolean {
    return p1 == null || p2 == null ? false : p1 == p2;
  }

  public cargarJuego(): void {
    this.activedRoute.params.subscribe(params => {
      let juegoRef = params["juegoRef"];
      if (juegoRef) {
        this.changeNuevoJuego();
        this.juegoService
          .getJuego(juegoRef)
          .subscribe(juego => (this.juego = juego));
      }
    });
  }

  public create(): void {
    this.juegoService.create(this.juego).subscribe(response => {
      this.router.navigate(["/juego"]);
      Swal.fire("Nuevo juego", "Juego se ha insertado con éxito", "success");
    });
  }

  public update(): void {
    this.juegoService.update(this.juego).subscribe(response => {
      this.router.navigate(["/juego"]);
      Swal.fire("Juego editado", "Juego modificado con éxito", "success");
    });
  }
}
