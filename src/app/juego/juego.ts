import { Company } from "../company/company";

export class Juego {
  juegoRef: number;
  title: string;
  launchDate: string;
  category: string;
  pegi: number;
  companyName: Company[];
}
